# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

image: docker:19.03.1

services:
    - docker:18.09.7-dind

variables:
    ALPINE_VERSION: 3.10.1

before_script:
    # Login to the GitLab Docker registry
    - >
        echo $CI_REGISTRY_PASSWORD |
        docker login
        --username $CI_REGISTRY_USER
        --password-stdin $CI_REGISTRY

build:
    stage: build
    except:
        - tags
        - master
    variables:
        ALPINE_VERSION: latest
    script:
        # Download the latest Docker image
        - docker pull $CI_REGISTRY_IMAGE:latest || true
        # Build a Docker image
        - >
            docker build
            --cache-from $CI_REGISTRY_IMAGE:latest
            --build-arg ALPINE_VERSION=$ALPINE_VERSION
            --build-arg BUILD_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
            --build-arg VCS_REF=$CI_COMMIT_SHORT_SHA
            .

latest:
    stage: deploy
    only:
        - master
    except:
        - tags
    environment:
        name: docker/latest
        url: $DOCKER_HUB_URL
    variables:
        ALPINE_VERSION: latest
    script:
        # Download the latest Docker image
        - docker pull $CI_REGISTRY_IMAGE:latest || true
        # Build a Docker image
        - >
            docker build
            --cache-from $CI_REGISTRY_IMAGE:latest
            --build-arg ALPINE_VERSION=$ALPINE_VERSION
            --build-arg BUILD_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
            --build-arg VCS_REF=$CI_COMMIT_SHORT_SHA
            --tag $CI_REGISTRY_IMAGE:latest
            .
        # Push to the GitLab Docker registry
        - docker push $CI_REGISTRY_IMAGE:latest

        # Create a latest tag
        - docker tag $CI_REGISTRY_IMAGE:latest $DOCKER_REGISTRY_IMAGE:latest
        # Login to the Docker Hub registry
        - >
            echo $DOCKER_REGISTRY_PASSWORD |
            docker login
            --username $DOCKER_REGISTRY_USER
            --password-stdin
        # Push to the Docker Hub registry
        - docker push $DOCKER_REGISTRY_IMAGE:latest

stable:
    stage: deploy
    only:
        - tags
    environment:
        name: docker/stable
        url: $DOCKER_HUB_URL
    script:
        # Set Docker image tag version
        - VERSION=$(echo $CI_COMMIT_REF_NAME | sed 's/^v//')
        # Download the latest Docker image
        - docker pull $CI_REGISTRY_IMAGE:latest || true
        # Build a Docker image
        - >
            docker build
            --cache-from $CI_REGISTRY_IMAGE:latest
            --build-arg ALPINE_VERSION=$ALPINE_VERSION
            --build-arg VERSION=$CI_COMMIT_REF_NAME
            --build-arg BUILD_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
            --build-arg VCS_REF=$CI_COMMIT_SHORT_SHA
            --tag $CI_REGISTRY_IMAGE:latest
            .
        # Create tags
        - docker tag $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:stable
        - docker tag $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$VERSION
        # Push to the GitLab Docker registry
        - docker push $CI_REGISTRY_IMAGE:latest
        - docker push $CI_REGISTRY_IMAGE:stable
        - docker push $CI_REGISTRY_IMAGE:$VERSION

        # Create tags
        - docker tag $CI_REGISTRY_IMAGE:latest $DOCKER_REGISTRY_IMAGE:latest
        - docker tag $CI_REGISTRY_IMAGE:latest $DOCKER_REGISTRY_IMAGE:stable
        - docker tag $CI_REGISTRY_IMAGE:latest $DOCKER_REGISTRY_IMAGE:$VERSION
        # Login to the Docker Hub registry
        - >
            echo $DOCKER_REGISTRY_PASSWORD |
            docker login
            --username $DOCKER_REGISTRY_USER
            --password-stdin
        # Push to the Docker Hub registry
        - docker push $DOCKER_REGISTRY_IMAGE:latest
        - docker push $DOCKER_REGISTRY_IMAGE:stable
        - docker push $DOCKER_REGISTRY_IMAGE:$VERSION
